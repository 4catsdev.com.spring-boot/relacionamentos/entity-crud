package com.fourcatsdev.entitycrud.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.entitycrud.modelo.Estudante;

public interface EstudanteRepositorio extends JpaRepository<Estudante, Long> {

}
